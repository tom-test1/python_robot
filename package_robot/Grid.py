#coding: utf-8 -*-

#export PYTHONPATH=$pythonpath:"."

class Grid:


    def __init__(self, nb_lignes, nb_colonnes):
        """ Creer une grille de 0 de taille nb_ligne*nb_colonnes, avec :

        Input : nb_lignes : entier >=1
                nb_colonnes : entier >=1

        Output :Grille de taille nb_lignes*nb_colonnes, initialisée à 0 partout sauf 1 en [0,0]
        """

        self.grid=[]
        self.nb_lignes = nb_lignes
        self.nb_colonnes = nb_colonnes
        for i in range(nb_lignes):
            self.grid.append([0]*nb_colonnes)
        self.coords = [0,0]
        self.grid[0][0]=1


    def __str__(self):
        return(str(self.grid).replace("],","]\n"))
    
    def peut_monter(self):
        peut_monter = True
        for i in range(self.nb_colonnes):
            if self.grid[0][i] == 1:
                peut_monter = False
        return peut_monter

    def peut_descendre(self):
        peut_descendre = True
        for i in range(self.nb_colonnes):
            if self.grid[self.nb_lignes-1][i] == 1:
                peut_descendre = False
        return peut_descendre

    def peut_droite(self):
        peut_droite = True
        for i in range(self.nb_lignes):
            if self.grid[i][self.nb_colonnes-1] == 1:
                peut_droite = False
        return peut_droite

    def peut_gauche(self):
        peut_gauche = True
        for i in range(self.nb_lignes):
            if self.grid[i][0] == 1:
                peut_gauche = False
        return peut_gauche


    def haut(self):
        if self.peut_monter():
            self.grid[self.coords[0]][self.coords[1]] = 0
            self.coords[0] = self.coords[0]-1
            self.grid[self.coords[0]][self.coords[1]] = 1
    
    def bas(self):
        if self.peut_descendre():
            self.grid[self.coords[0]][self.coords[1]] = 0
            self.coords[0] = self.coords[0]+1
            self.grid[self.coords[0]][self.coords[1]] = 1    

    def gauche(self):
        if self.peut_gauche():
            self.grid[self.coords[0]][self.coords[1]] = 0
            self.coords[1] = self.coords[1]-1
            self.grid[self.coords[0]][self.coords[1]] = 1    

    def droite(self):
        if self.peut_droite():
            self.grid[self.coords[0]][self.coords[1]] = 0
            self.coords[1] = self.coords[1]+1
            self.grid[self.coords[0]][self.coords[1]] = 1   

